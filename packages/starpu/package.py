##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
from spack import *
import platform
import os
import subprocess

class Starpu(AutotoolsPackage):
    """StarPU runtime system"""
    homepage = "http://starpu.gforge.inria.fr/"

    version('1.3.3', 'f14d2230864ac51b212494ce3aa1dd9c',
             url="http://starpu.gforge.inria.fr/files/starpu-1.3.3/starpu-1.3.3.tar.gz")
    version('1.3.2', 'e10967a0decc2f448e00b741ee0f7d90',
             url="http://starpu.gforge.inria.fr/files/starpu-1.3.2/starpu-1.3.2.tar.gz")
    version('1.3.1', '6b94c705ca73c2efca49433904bd105d',
             url="http://starpu.gforge.inria.fr/files/starpu-1.3.1/starpu-1.3.1.tar.gz")
    version('1.3.0', '2dd4f2bb61d34747f69fc124d97e764e',
             url="http://starpu.gforge.inria.fr/files/starpu-1.3.0/starpu-1.3.0.tar.gz")
    version('1.2.8', '750ff24417a7d59eba695cc18b00e201',
             url="http://starpu.gforge.inria.fr/files/starpu-1.2.8/starpu-1.2.8.tar.gz")
    version('1.2.7', '94cf2d8d54819ae5851c96471e742d97',
             url="http://starpu.gforge.inria.fr/files/starpu-1.2.7/starpu-1.2.7.tar.gz")
    version('1.2.6', '053079a24b4f76398251ed5660249ad8',
             url="http://starpu.gforge.inria.fr/files/starpu-1.2.6/starpu-1.2.6.tar.gz")
    version('1.2.5', 'a372108458074e9335c91014fc0307b4',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.5/starpu-1.2.5.tar.gz")
    version('1.2.4', 'f1ad9f042acdc7f2a96c31fab0409728',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.4/starpu-1.2.4.tar.gz")
    version('1.2.3', '1db2bb0dc07229a3a457cf1573841afa',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.3/starpu-1.2.3.tar.gz")
    version('1.2.2', '08c11c656c0df646aed243ed26724683',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.2/starpu-1.2.2.tar.gz")
    version('1.2.1', '9f04db940cfebb737241c4d4b2adcc92',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.1/starpu-1.2.1.tar.gz")
    version('1.2.0', '0cc98ac39b9cb4083c6c51399029d33b',
            url="http://starpu.gforge.inria.fr/files/starpu-1.2.0/starpu-1.2.0.tar.gz")
    version('1.1.6', '005a3c15b25cb36df09e2492035b5aad',
            url="http://starpu.gforge.inria.fr/files/starpu-1.1.6/starpu-1.1.6.tar.gz")

    version('develop', git='https://scm.gforge.inria.fr/anonscm/git/starpu/starpu.git', branch='master')
    version('master', git='https://scm.gforge.inria.fr/anonscm/git/starpu/starpu.git', branch='starpu-1.2')
    version('git-1.1', git='https://scm.gforge.inria.fr/anonscm/git/starpu/starpu.git', branch='starpu-1.1')
    version('git-1.2', git='https://scm.gforge.inria.fr/anonscm/git/starpu/starpu.git', branch='starpu-1.2')
    version('git-1.3', git='https://scm.gforge.inria.fr/anonscm/git/starpu/starpu.git', branch='starpu-1.3')

    variant('shared', default=True, description='Build STARPU as a shared library')
    variant('fast', default=True, description='Disable runtime assertions')
    variant('debug', default=False, description='Enable debug symbols')
    variant('verbose', default=False, description='Enable verbose debugging')
    variant('fxt', default=False, description='Enable FxT tracing support')
    variant('mpi', default=True, description='Enable MPI support')
    variant('cuda', default=False, description='Enable CUDA support')
    variant('opencl', default=False, description='Enable OpenCL support')
    variant('openmp', default=True, description='Enable OpenMP support')
    variant('fortran', default=False, description='Enable Fortran interface and examples')
    variant('simgrid', default=False, description='Enable SimGrid support')
    variant('simgridmc', default=False, description='Enable SimGrid model checker support')
    variant('examples', default=True, description='Enable Examples')
    variant('nmad', default=False, description='Enable StarPU-MPI implementation on top of NewMadeleine')
    variant('poti', default=False, description='Enable the use of poti paje traces')

    depends_on("hwloc@:1.999")
    depends_on("hwloc+cuda", when='+cuda')
    depends_on("mpi", when='+mpi~simgrid')
    depends_on("cuda", when='+cuda~simgrid')
    depends_on("fxt", when='+fxt')
    depends_on("simgrid", when='+simgrid')
    depends_on("simgrid+smpi", when='+simgrid+mpi')
    depends_on("simgrid+mc", when='+simgridmc')
    depends_on("nmad+mpi+pioman", when='+nmad')
    depends_on("poti", when='+poti')

    conflicts('+nmad', when="@1.1", msg="nmad is not available with branch 1.1")
    conflicts('+nmad', when="@1.2", msg="nmad is not available with branch 1.2")
    conflicts('+poti', when='~fxt', msg="Poti needs fxt")
    conflicts('+shared', when='+mpi+simgrid', msg="Simgrid MPI require to disable shared library")

    def autoreconf(self, spec, prefix):
        if not os.path.isfile("./configure"):
            autogen = Executable("./autogen.sh")
            autogen()

    def configure_args(self):
        spec = self.spec

        config_args = [
            '--disable-build-doc',
            '--enable-blas-lib=none',
            '--disable-mlr',
        ]

        # add missing lib for simgrid static compilation, already fixed since StarPU 1.2.1
        if spec.satisfies('+fxt'):
            mf = FileFilter('configure')
            mf.filter('libfxt.a -lrt', 'libfxt.a -lrt -lbfd')

        mpicc = ""
        if spec.satisfies('+mpi~simgrid'):
            mpicc = spec['mpi'].mpicc
        elif spec.satisfies('+mpi+simgrid'):
            mpicc = "%s/bin/smpicc" % spec['simgrid'].prefix

        config_args.extend([
            "--%s-shared"         % ('enable' if '+shared'   in spec else 'disable'),
            "--%s-debug"          % ('enable' if '+debug'    in spec else 'disable'),
            "--%s-verbose"        % ('enable' if '+verbose'  in spec else 'disable'),
            "--%s-fast"           % ('enable' if '+fast'     in spec else 'disable'),

            "--%s-build-tests"    % ('enable' if '+examples' in spec else 'disable'),
            "--%s-build-examples" % ('enable' if '+examples' in spec else 'disable'),

            "--%s-fortran"        % ('enable' if '+fortran'  in spec else 'disable'),
            "--%s-openmp"         % ('enable' if '+openmp'   in spec else 'disable'),

            "--%s-opencl"         % ('disable' if '~opencl' in spec or '+simgrid' in spec else 'enable'),
            "--%s-cuda"           % ('disable' if '~cuda'   in spec or '+simgrid' in spec else 'enable'),

            "--without-mpicc"     if '~mpi'  in spec else "--with-mpicc=%s" % mpicc,
            "--enable-nmad"       if '+nmad' in spec else "",
            "--enable-poti"       if '+poti' in spec else "",

            "--with-hwloc=%s"     % spec['hwloc'].prefix,
        ])

        if spec.satisfies('+fxt'):
            config_args.append("--with-fxt=%s" % spec['fxt'].prefix)
            if spec.satisfies('@1.2:') or spec.satisfies('@svn-1.2') or spec.satisfies('@svn-trunk') or spec.satisfies('@git-1.2'):
                config_args.append("--enable-paje-codelet-details")

        if spec.satisfies('+simgrid'):
            config_args.append("--enable-simgrid")
            config_args.append("--with-simgrid-dir=%s" % spec['simgrid'].prefix)
            if spec.satisfies('+simgridmc'):
                config_args.append("--enable-simgrid-mc")

        # On OSX, deactivate glpk
        if platform.system() == 'Darwin':
            config_args.append("--disable-glpk")

        return config_args
