##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *
import os
import getpass
import spack

class Aerosol(CMakePackage):
    """AeroSol is a high order Finite Element C++ library. It requires an MPI
       implementation, Pampa (therefore also Scotch), BLAS or Eigen3/IMKL,
       libXML2 and can also utilize HDF5 and PAPI.

       As there is no publically released version of AeroSol yet, this package
       requires the user has access to Aerosol's repository on gitlab.inria.fr."""

    # GitLab repo
    homepage = "https://gitlab.inria.fr/aerosol/aerosol"
    url      = "https://gitlab.inria.fr/aerosol/aerosol/repository/master/archive.tar.bz2"
    gitroot  = "git@gitlab.inria.fr:aerosol/aerosol.git"

    version('master', git=gitroot, branch='master', preferred=True)

    # variant
    variant('hdf5',    default=False, description='Enable IO using parallel HDF5')
    variant('umfpack', default=False, description='Enable UMFPACK linear solver')
    variant('petsc',   default=False, description='Enable PETSc linear solvers')
    variant('mumps',   default=False, description='Enable MUMPS linear solver')
    variant('pastix',  default=False, description='Enable PaStix linear solver')
    variant('extra',   default=False, description='Enable PAPI and Doxygen usage')

    # required dependencies
    depends_on('cmake')
    depends_on('libxml2')
    depends_on('mpi')
    depends_on('blas')
    depends_on('lapack')
    depends_on('pampa~thread')

    # optional dependencies
    depends_on('hdf5+mpi',                             when='+hdf5')
    depends_on('suite-sparse',                         when='+umfpack')
    depends_on('petsc+hypre~mumps~superlu-dist',       when='+petsc')
    depends_on('mumps+scotch+ptscotch+metis+parmetis', when='+mumps')
    depends_on('pastix~metis',                         when='+pastix')
    depends_on('papi',                                 when='+extra')
    depends_on('doxygen',                              when='+extra')

    def cmake_args(self):
        # We try to keep CMake default as much as possible
        spec = self.spec
        args = []

        args.extend([
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"])

        args.extend(["-DMPI_C_COMPILER=%s" % spec['mpi'].mpicc])
        args.extend(["-DMPI_CXX_COMPILER=%s" % spec['mpi'].mpicxx])
        args.extend(["-DMPI_Fortran_COMPILER=%s" % spec['mpi'].mpifc])

        args.extend(["-DWITH_XML2=ON"])

        if spec.satisfies('+hdf5'):
            args.extend(["-DWITH_HDF5=ON"])

        if spec.satisfies('^intel-mkl') or spec.satisfies('^intel-parallel-studio+mkl'):
            args.extend(["-DDENSE_LINEAR_ALGEBRA_PACKAGE=MKL"])
        elif spec.satisfies('^netlib-lapack'):
            args.extend(["-DDENSE_LINEAR_ALGEBRA_PACKAGE=NETLIB"])
        elif spec.satisfies('^openblas'):
            args.extend(["-DDENSE_LINEAR_ALGEBRA_PACKAGE=OPENBLAS"])
        else:
            raise RuntimeError("BLAS/LAPACK vendor not supported. Should be one of intel-mkl, intel-parallel-studio+mkl, netlib-lapack, openblas.")

        # Configure sparse linear solvers
        args.extend(["-DWITH_BLOCKDIAGONALSOLVER=ON"])
        args.extend(["-DWITH_DIAGONALSOLVER=ON"])

        if spec.satisfies('+umfpack'):
            args.extend(["-DWITH_UMFPACK=ON"])
        if spec.satisfies('+petsc'):
            args.extend(["-DWITH_PETSC=ON"])
        # Hypre isn't a variant, maybe put petsc ^hypre here
        if spec.satisfies('+petsc+hypre'):
            args.extend(["-DWITH_PETSC_HYPRE=ON"])
        if spec.satisfies('+mumps'):
            args.extend(["-DWITH_MUMPS=ON"])
        if spec.satisfies('+pastix'):
            args.extend(["-DWITH_PASTIX=ON"])

        # Other configuration
        if spec.satisfies('+extra'):
            args.extend(["-DWITH_PAPI=ON"])
            args.extend(["-DWITH_SIMULATIONS=ON"])
            args.extend(["-DWITH_TEST=ON"])

        return args
