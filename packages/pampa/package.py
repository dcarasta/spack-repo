##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *

class Pampa(CMakePackage):
    """PaMPA is a middleware that manages distributed meshes. It allows
       users to write parallel solver codes without having to bother about
       data exchange, data redistribution, remeshing and load balancing."""
    homepage = "https://project.inria.fr/pampa/"
    url      = "https://gitlab.inria.fr/PaMPA/PaMPA/repository/master/archive.tar.bz2"
    gitroot  = "https://gitlab.inria.fr/PaMPA/PaMPA.git"

    version('master' , git=gitroot, branch='master', preferred=True)

    variant('shared', default=True,
            description='Build shared libs')
    variant('thread', default=True,
            description='Enable multi-threaded algorithms')
    variant('int64', default=False,
            description='64 bits integer')

    depends_on('cmake')
    depends_on('mpi')
    depends_on('scotch~metis+mpi')
    depends_on('scotch~metis+mpi+int64', when='+int64')

    def cmake_args(self):

        spec = self.spec
        args = []

        args.extend([
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"])
        args.extend(["-DBUILD_SHARED_LIBS=%s" % ('ON' if '+shared' in spec else 'OFF')])

        args.extend(["-DMPI_C_COMPILER=%s" % spec['mpi'].mpicc])
        args.extend(["-DMPI_CXX_COMPILER=%s" % spec['mpi'].mpicxx])
        args.extend(["-DMPI_Fortran_COMPILER=%s" % spec['mpi'].mpifc])
        args.extend(["-DCOMM_TYPE=Point-to-point"])
        args.extend(["-DPAMPA_MULTILEVEL=ON"])

        if spec.satisfies('~thread'):
            args.extend(["-DPTHREAD=None"])
        args.extend(["-DSAMPLES=False"])
        if spec.satisfies('+int64'):
            args.extend(["-DINT_SIZE=64bits"])

        return args
