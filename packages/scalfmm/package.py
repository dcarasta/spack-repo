##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################

from spack import *

class Scalfmm(CMakePackage):
    """N-body interactions using the Fast Multipole Method"""

    homepage = "https://gitlab.inria.fr/solverstack/ScalFMM"
    url      = "https://gitlab.inria.fr/solverstack/ScalFMM"
    gitroot  = "https://gitlab.inria.fr/solverstack/ScalFMM.git"

    version('1.5' , git=gitroot, branch='maintenance/scalfmm-1.5', preferred=True)
    version('develop', git=gitroot, branch='develop')
    version('master', git=gitroot, branch='maintenance/scalfmm-1.5')

    variant('debug', default=False, description='Enable debug symbols')
    variant('shared', default=True, description='Build scalfmm as a shared library')
    variant('sse', default=True, description='Enable vectorization with SSE')
    variant('fft', default=True, description='Enable FFT')
    variant('mpi', default=True, description='Enable MPI')
    variant('starpu', default=False, description='Enable StarPU')
    variant('examples', default=True, description='Enable compilation and installation of example executables')
    variant('tests', default=False, description='Enable compilation and installation of test executables (Tests repository)')

    depends_on("blas")
    depends_on("lapack")
    depends_on("mpi", when='+mpi')
    depends_on("fftw", when='+fft~mpi')
    depends_on("fftw+mpi", when='+fft+mpi')
    depends_on("starpu", when='+starpu~mpi')
    depends_on("starpu+mpi", when='+mpi+starpu')

    def cmake_args(self):
        spec = self.spec

        args = []

        args.extend([
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON",
            "-DSCALFMM_BUILD_DEBUG=%s"    % ('ON'    if '+debug'    in spec else 'OFF'),
            "-DBUILD_SHARED_LIBS=%s"      % ('ON'    if '+shared'   in spec else 'OFF'),
            "-DSCALFMM_BUILD_EXAMPLES=%s" % ('ON'    if '+examples' in spec else 'OFF'),
            "-DSCALFMM_BUILD_TESTS=%s"    % ('ON'    if '+examples' in spec else 'OFF'),
            "-DSCALFMM_INSTALL_DATA=%s"   % ('ON'    if '+tests'    in spec else 'OFF'),
            "-DSCALFMM_BUILD_UTESTS=%s"   % ('ON'    if '+tests'    in spec else 'OFF'),
            "-DSCALFMM_INSTALL_DATA=%s"   % ('ON'    if '+tests'    in spec else 'OFF'),
            "-DSCALFMM_USE_SSE=%s"        % ('ON'    if '+sse'      in spec else 'OFF'),
            "-DSCALFMM_USE_FFT=%s"        % ('ON'    if '+fft'      in spec else 'OFF'),
            "-DSCALFMM_USE_MPI=%s"        % ('ON'    if '+mpi'      in spec else 'OFF'),
            "-DSCALFMM_USE_STARPU=%s"     % ('ON'    if '+starpu'   in spec else 'OFF')
            ])

        # Blas
        blas_libs = spec['blas'].libs.ld_flags
        args.extend(["-DBLAS_LIBRARIES=%s" % blas_libs])

        # Lapack
        lapack_libs = spec['lapack'].libs.ld_flags
        args.extend(["-DLAPACK_LIBRARIES=%s" % lapack_libs])

        return args
