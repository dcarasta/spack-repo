##############################################################################
# Copyright (c) 2020, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *
import os
import sys

def get_submodules():
    if os.path.exists(".git"):
        git = which('git')
        git('submodule', 'update', '--init', '--recursive')

class Chameleon(CMakePackage):
    """Dense Linear Algebra for Scalable Multi-core Architectures and GPGPUs"""
    homepage = "https://gitlab.inria.fr/solverstack/chameleon"

    version('1.0.0', '0eb9e5ffd0c81f63dba54bec8bd2d66d',
            url='https://gitlab.inria.fr/solverstack/chameleon/uploads/6115a9dd4dc21da4aed13a14b2de49d3/chameleon-1.0.0.tar.gz')
    version('0.9.2', 'd697b06aa82af9fb7ea242d517c7c471',
            url='https://gitlab.inria.fr/solverstack/chameleon/uploads/60df876daea9498a260c4e5bcf41b84c/chameleon-0.9.2.tar.gz')
    version('master', git='https://gitlab.inria.fr/solverstack/chameleon.git', submodules=True)

    # cmake's specific
    variant('debug', default=False, description='Enable debug symbols')
    variant('shared', default=True, description='Build chameleon as a shared library')

    # chameleon's specific
    variant('starpu', default=True, description='Use StarPU runtime')
    variant('quark', default=False, description='Use Quark runtime instead of StarPU')
    variant('openmp', default=False, description='Use OpenMP runtime instead of StarPU')
    variant('mpi', default=True, description='Enable MPI')
    variant('cuda', default=False, description='Enable CUDA')
    variant('migrate', default=True, description='Enable the data migration in QR algorithms')
    variant('fxt', default=False, description='Enable FxT tracing support through StarPU')
    variant('simgrid', default=False, description='Enable simulation mode through StarPU+SimGrid')

    # dependencies
    depends_on("cmake")
    depends_on("blas", when='~simgrid')
    depends_on("lapack", when='~simgrid')
    depends_on("starpu", when='+starpu')
    depends_on("starpu~mpi", when='+starpu~mpi')
    depends_on("starpu+cuda", when='+starpu+cuda~simgrid')
    depends_on("starpu+fxt", when='+starpu+fxt')
    depends_on("starpu+simgrid", when='+starpu+simgrid')
    depends_on("starpu+mpi~shared+simgrid", when='+starpu+simgrid+mpi')
    depends_on("quark", when='+quark')
    depends_on("mpi", when='+mpi~simgrid')
    depends_on("cuda", when='+cuda~simgrid')
    depends_on("fxt", when='+fxt+starpu')

    # handle conflict cases
    conflicts('+quark', when='+starpu', msg="You have to choose one and only one runtime system to schedule tasks, please disable either quark or starpu.")
    conflicts('+quark', when='+openmp', msg="You have to choose one and only one runtime system to schedule tasks, please disable either quark or openmp.")
    conflicts('+openmp', when='+starpu', msg="You have to choose one and only one runtime system to schedule tasks, please disable either openmp or starpu.")

    @run_before('cmake')
    def update_submodules(self):

        spec = self.spec

        if spec.satisfies('@master'):
            if not os.path.exists("cmake_modules/morse_cmake/modules"):
                get_submodules()


    def cmake_args(self):

        spec = self.spec
        args = []

        args.extend([
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON",
            "-DCHAMELEON_ENABLE_EXAMPLE=ON",
            "-DCHAMELEON_ENABLE_TESTING=ON",
            "-DCMAKE_BUILD_TYPE=%s "         % ('Debug' if '+debug' in spec else 'Release'),
            "-DBUILD_SHARED_LIBS=%s "        % ('ON' if '+shared' in spec else 'OFF'),
            "-DCHAMELEON_USE_MPI=%s "        % ('ON' if '+mpi' in spec else 'OFF'),
            "-DCHAMELEON_USE_CUDA=%s "       % ('ON' if '+cuda' in spec else 'OFF'),
            "-DCHAMELEON_USE_MIGRATE=%s "    % ('ON' if '+migrate' in spec else 'OFF'),
            "-DCHAMELEON_SIMULATION=%s "     % ('ON' if '+simgrid' in spec else 'OFF')
        ])

        if spec.satisfies('+quark'):
            args.extend(["-DCHAMELEON_SCHED=QUARK"])
        if spec.satisfies('+openmp'):
            args.extend(["-DCHAMELEON_SCHED=OPENMP"])
        if spec.satisfies('+starpu'):
            args.extend(["-DCHAMELEON_SCHED=STARPU"])
        if spec.satisfies('~quark') and spec.satisfies('~openmp') and spec.satisfies('~starpu'):
            raise InstallError('Chameleon requires to have one runtime system enabled, please enable either quark, openmp or starpu.')

        if spec.satisfies('+mpi+simgrid'):
            args.extend(["-DMPI_C_COMPILER=%s" % self.spec['simgrid'].smpicc])
            args.extend(["-DMPI_CXX_COMPILER=%s" % self.spec['simgrid'].smpicxx])
            args.extend(["-DMPI_Fortran_COMPILER=%s" % self.spec['simgrid'].smpifc])

        if spec.satisfies('+mpi~simgrid'):
            args.extend(["-DMPI_C_COMPILER=%s" % self.spec['mpi'].mpicc])
            args.extend(["-DMPI_CXX_COMPILER=%s" % self.spec['mpi'].mpicxx])
            args.extend(["-DMPI_Fortran_COMPILER=%s" % self.spec['mpi'].mpifc])

        if spec.satisfies('~simgrid'):
            if '^atlas' in spec:
                raise InstallError('Chameleon requires to have a full BLAS LAPACK implementation, with CBLAS, LAPACKE and TMGLIB. ATLAS cannot be used.')
                #args.extend(["-DBLA_VENDOR=ATLAS"])
            elif '^intel-mkl' in spec or '^intel-parallel-studio+mkl' in spec:
                if '^intel-mkl threads=none' in spec or '^intel-parallel-studio threads=none' in spec:
                    args.extend(["-DBLA_VENDOR=Intel10_64lp_seq"])
                else:
                    args.extend(["-DBLA_VENDOR=Intel10_64lp"])
            elif '^netlib-lapack' in spec:
                args.extend(["-DBLA_VENDOR=Generic"])
            elif '^openblas' in spec:
                args.extend(["-DBLA_VENDOR=Open"])
            elif '^veclibfort' in spec:
                raise InstallError('Chameleon with veclibfort has never been tested. Please select another BLAS/LAPACK provider.')
                #args.extend(["-DBLA_VENDOR=Apple"])

        return args
