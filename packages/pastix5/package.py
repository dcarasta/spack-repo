##############################################################################
# Copyright (c) 2018, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *
from shutil import copyfile
import urllib
import os

class Pastix5(CMakePackage):
    """a high performance parallel solver for very large sparse linear systems based on direct methods"""

    homepage = "http://pastix.gforge.inria.fr/files/README-txt.html"
    url      = "https://gforge.inria.fr/frs/download.php/file/36212/pastix_5.2.3.tar.bz2"

    version('5.2.3', '31a1c3ea708ff2dc73280e4b85a82ca8',
            url='https://gforge.inria.fr/frs/download.php/file/36212/pastix_5.2.3.tar.bz2')
    version('develop', git='https://scm.gforge.inria.fr/anonscm/git/ricar/ricar.git', branch='develop')

    # Variants

    variant('mpi', default=True, description='Enable MPI')
    variant('smp', default=True, description='Enable Thread')
    variant('blasmt', default=False, description='Enable to use multithreaded Blas library (MKL, ESSL, OpenBLAS)')
    variant('cuda', default=False, description='Enable CUDA kernels. Caution: only available if StarPU variant is enabled')
    variant('metis', default=True, description='Enable Metis')
    variant('scotch', default=True, description='Enable Scotch')
    variant('starpu', default=False, description='Enable StarPU')
    variant('shared', default=True, description='Build Pastix as a shared library')
    variant('examples', default=True, description='Enable compilation and installation of example executables')
    variant('debug', default=False, description='Enable debug symbols')
    variant('int64', default=False, description='To use 64 bits integers')
    variant('dynsched', default=False, description='Enable dynamic thread scheduling support')
    variant('memory', default=True, description='Enable memory usage statistics')
    variant('murgeup', default=False, description='Pull git murge source code (internet connection required), useful for the develop branch')
    # TODO
    #variant('pypastix', default=False, description='Create a python 2 wrapper for pastix called pypastix')
    #variant('pypastix3', default=False, description='Create a python 3 wrapper for pastix called pypastix')

    # Dependencies

    depends_on("cmake@3.3:")
    depends_on("hwloc@:1.999")
    depends_on("hwloc+cuda", when='+cuda')
    depends_on("mpi", when='+mpi')
    depends_on("blas")
    depends_on("scotch~metis", when='+scotch')
    depends_on("scotch~metis~mpi", when='+scotch~mpi')
    depends_on("scotch~metis+int64", when='+scotch+int64')
    depends_on("metis@5.1:", when='+metis')
    depends_on("metis@5.1:+int64", when='+metis+int64')
    depends_on("starpu", when='+starpu')
    depends_on("starpu~mpi", when='+starpu~mpi')
    depends_on("starpu+cuda", when='+starpu+cuda')

    @property
    def root_cmakelists_dir(self):
        if '@solverstack' not in self.spec:
            return os.path.join(self.stage.source_path, 'src')
        return self.stage.source_path

    def cmake_args(self):
        spec = self.spec

        src_dir = os.path.join(self.stage.source_path, 'src')
        os.chdir(src_dir)
        # pre-processing due to the dependency to murge (git)
        copyfile('config/LINUX-GNU.in', 'config.in')
        if spec.satisfies('+murgeup'):
            # required to get murge sources "make murge_up"
            make('murge_up')
        # this file must be generated
        make('sopalin/src/murge_fortran.c')

        args = [
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DBUILD_SHARED_LIBS=%s"     % ('ON'    if '+shared'   in spec else 'OFF'),
            "-DPASTIX_WITH_MPI=%s"       % ('ON'    if '+mpi'      in spec else 'OFF'),
            "-DPASTIX_WITH_STARPU=%s"    % ('ON'    if '+starpu'   in spec else 'OFF'),
            "-DPASTIX_WITH_CUDA=%s"      % ('ON'    if '+cuda'     in spec else 'OFF'),
            "-DPASTIX_ORDERING_METIS=%s" % ('ON'    if '+metis'    in spec else 'OFF'),
            "-DPASTIX_INT64=%s"          % ('ON'    if '+int64'    in spec else 'OFF'),
        ]

        args.extend([
            "-DPASTIX_WITH_MULTITHREAD=%s"  % ('ON' if '+smp'             in spec else 'OFF'),
            "-DPASTIX_DYNSCHED=%s"          % ('ON' if '+dynsched'        in spec else 'OFF'),
            "-DPASTIX_WITH_MEMORY_USAGE=%s" % ('ON' if '+memory'          in spec else 'OFF'),
            "-DPASTIX_DISTRIBUTED=%s"       % ('ON' if '^scotch+mpi'      in spec else 'OFF'),
            "-DBLA_STATIC=%s"               % ('ON' if '^mkl-blas~shared' in spec else 'OFF'),
            "-DPASTIX_BLAS_MT=%s"           % ('ON' if '+blasmt'          in spec else 'OFF'),
        ])

        blas_libs = spec['blas'].libs.ld_flags
        args.extend(["-DBLAS_LIBRARIES=%s" % blas_libs])

        # It seems sometimes cmake needs some help with that
        if 'mkl_intel_lp64' in blas_libs:
            args.extend(["-DBLA_VENDOR=Intel10_64lp"])

        if spec.satisfies("%xl"):
            args.extend(["-DCMAKE_C_FLAGS=-qstrict -qsmp -qlanglvl=extended -qarch=auto -qhot -qtune=auto"])
            args.extend(["-DCMAKE_Fortran_FLAGS=-qstrict -qsmp -qarch=auto -qhot -qtune=auto"])
            args.extend(["-DCMAKE_CXX_FLAGS=-qstrict -qsmp -qlanglvl=extended -qarch=auto -qhot -qtune=auto"])
            args.extend(["-DPASTIX_FM_NOCHANGE=ON"])

        return args
