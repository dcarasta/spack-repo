##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *

class Parsec(CMakePackage):
    """Parallel Runtime Scheduling and Execution Controller."""
    homepage = "https://bitbucket.org/mfaverge/parsec"

    version ('mfaverge', git='https://bitbucket.org/mfaverge/parsec.git', branch='mymaster')

    variant('debug', default=False, description='Enable debug symbols')
    variant('shared', default=True, description='Enable shared library')
    variant('cuda', default=False, description='Enable CUDA support')
    variant('mpi', default=False, description='Enable MPI support')

    depends_on("cmake")
    depends_on("hwloc")
    depends_on("cuda", when='+cuda')
    depends_on("mpi", when='+mpi')

    def cmake_args(self):

        spec = self.spec
        args = []

        args.extend([
            "-Wno-dev",
            "-DCMAKE_BUILD_TYPE=%s "         % ('Debug' if '+debug' in spec else 'Release'),
            "-DBUILD_SHARED_LIBS=%s "        % ('ON' if '+shared' in spec else 'OFF'),
            "-DPARSEC_GPU_WITH_CUDA=%s "     % ('ON' if '+cuda' in spec else 'OFF'),
            "-DPARSEC_DIST_WITH_MPI=%s "     % ('ON' if '+mpi' in spec else 'OFF')
        ])

        return args
