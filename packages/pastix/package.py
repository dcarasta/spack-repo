##############################################################################
# Copyright (c) 2018, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *
from shutil import copyfile
import urllib
import os

class Pastix(CMakePackage):
    """a high performance parallel solver for very large sparse linear systems based on direct methods"""

    homepage = "https://gitlab.inria.fr/solverstack/pastix/blob/master/README.md"
    url      = "https://gforge.inria.fr/frs/download.php/file/37632/pastix-6.0.1.tar.gz"

    version('6.0.1', '276fff4e95ef0bad1402a7ad40d558722c509e3cc0193fbe509887c639227a30',
            url='https://gforge.inria.fr/frs/download.php/file/37632/pastix-6.0.1.tar.gz')
    version('solverstack', git="https://gitlab.inria.fr/solverstack/pastix.git", submodules=True, preferred=True)

    # Variants
    variant('debug', default=False, description='Enable debug symbols')
    variant('shared', default=True, description='Build Pastix as a shared library')
    variant('doc', default=False, description='Enable compilation and installation of the documentation')
    variant('int64', default=False, description='To use 64 bits integers')
    variant('metis', default=False, description='Enable Metis')
    variant('scotch', default=True, description='Enable Scotch')
    variant('parsec', default=False, description='Enable PaRSEC')
    variant('starpu', default=False, description='Enable StarPU')
    variant('mpi', default=False, description='Enable MPI')
    # TODO
    #variant('pypastix', default=False, description='Create a python 2 wrapper for pastix called pypastix')
    #variant('pypastix3', default=False, description='Create a python 3 wrapper for pastix called pypastix')

    # Dependencies

    depends_on("cmake@3.3:")
    depends_on("hwloc@:1.999")
    depends_on("lapack")
    depends_on("mpi", when='+mpi')
    depends_on("scotch~metis", when='+scotch')
    depends_on("scotch~metis~mpi", when='+scotch~mpi')
    depends_on("scotch~metis+int64", when='+scotch+int64')
    depends_on("metis@5.1:", when='+metis')
    depends_on("metis@5.1:+int64", when='+metis+int64')
    depends_on("parsec", when='+parsec')
    depends_on("starpu", when='+starpu')

    def cmake_args(self):
        spec = self.spec

        args = [
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DBUILD_SHARED_LIBS=%s"      % ('ON'    if '+shared'   in spec else 'OFF'),
            "-DBUILD_DOCUMENTATION=%s"    % ('ON'    if '+doc'      in spec else 'OFF'),
            "-DPASTIX_INT64=%s"           % ('ON'    if '+int64'    in spec else 'OFF'),
            "-DPASTIX_ORDERING_METIS=%s"  % ('ON'    if '+metis'    in spec else 'OFF'),
            "-DPASTIX_ORDERING_SCOTCH=%s" % ('ON'    if '+scotch'   in spec else 'OFF'),
            "-DPASTIX_WITH_PARSEC=%s"     % ('ON'    if '+parsec'   in spec else 'OFF'),
            "-DPASTIX_WITH_STARPU=%s"     % ('ON'    if '+starpu'   in spec else 'OFF'),
            "-DPASTIX_WITH_MPI=%s"        % ('ON'    if '+mpi'      in spec else 'OFF'),
        ]

        if '^atlas' in spec:
            raise InstallError('PaStiX requires to have a full BLAS LAPACK implementation, with CBLAS, LAPACKE and TMGLIB. ATLAS cannot be used.')
            #args.extend(["-DBLA_VENDOR=ATLAS"])
        elif '^intel-mkl' in spec or '^intel-parallel-studio+mkl' in spec:
            if '^intel-mkl threads=none' in spec or '^intel-parallel-studio threads=none' in spec:
                args.extend(["-DBLA_VENDOR=Intel10_64lp_seq"])
            else:
                args.extend(["-DBLA_VENDOR=Intel10_64lp"])
        elif '^netlib-lapack' in spec:
            args.extend(["-DBLA_VENDOR=Generic"])
        elif '^openblas' in spec:
            args.extend(["-DBLA_VENDOR=Open"])
        elif '^veclibfort' in spec:
            raise InstallError('PaStiX with veclibfort has never been tested.')
            #args.extend(["-DBLA_VENDOR=Apple"])

        if spec.satisfies("%xl"):
            args.extend(["-DCMAKE_C_FLAGS=-qstrict -qsmp -qlanglvl=extended -qarch=auto -qhot -qtune=auto"])
            args.extend(["-DCMAKE_Fortran_FLAGS=-qstrict -qsmp -qarch=auto -qhot -qtune=auto"])
            args.extend(["-DCMAKE_CXX_FLAGS=-qstrict -qsmp -qlanglvl=extended -qarch=auto -qhot -qtune=auto"])

        return args
