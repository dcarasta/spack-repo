##############################################################################
# Copyright (c) 2013-2016, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory.
#
# This file is part of Spack.
# Created by Todd Gamblin, tgamblin@llnl.gov, All rights reserved.
# LLNL-CODE-647188
#
# For details, see https://github.com/llnl/spack
# Please also see the NOTICE and LICENSE files for our notice and the LGPL.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License (as
# published by the Free Software Foundation) version 2.1, February 1999.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the terms and
# conditions of the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##############################################################################
#
from spack import *
import os
import platform
import spack
from shutil import copyfile

class OldQrMumps(Package):
    """a software package for the solution of sparse, linear systems on multicore computers based on the QR factorization of the input matrix."""
    homepage = "http://buttari.perso.enseeiht.fr/qr_mumps/"

    version('2.0', 'e880670f4dddba16032f43faa03aa903',
                    url="http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-2.0.tgz")
    version('1.2', '71d2b53e24aab3e68c4a4486911fe8cf',
                    url="http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-1.2.tgz")
    version('devel', svn='https://www.irit.fr/svn/qr_mumps/branches/qrm_gpu')

    variant('debug', default=False, description='Enable debug symbols')
    variant('cuda', default=False, description='Enable CUDA')
    variant('metis', default=True, description='Enable Metis ordering')
    variant('scotch', default=False, description='Enable Scotch ordering')
    variant('starpu', default=True, description='Enable StarPU runtime system support')
    variant('fxt', default=False, description='Enable FxT tracing support to be used through StarPU')
    variant('d', default=True, description='Build QrMumps in double precision')
    variant('s', default=False, description='Build QrMumps in simple precision')
    variant('z', default=False, description='Build QrMumps in complex double precision')
    variant('c', default=False, description='Build QrMumps in complex simple precision')

    # dependencies
    depends_on("cmake",when='@devel')
    depends_on("blas",type=('run'))
    depends_on("lapack",type=('run'))
    # optional dependencies
    depends_on("starpu~mpi", when='+starpu')
    depends_on("starpu~mpi+fxt", when='+starpu+fxt')
    depends_on("starpu@svn-trunk", when='@devel+starpu')
    depends_on("metis", when='+metis')
    depends_on("scotch~mpi", when='+scotch')
    # dependencies for gpu
    depends_on("cuda", when='@devel+cuda')
    depends_on("starpu+cuda", when='@devel+starpu@svn-trunk+cuda')
    depends_on("starpu+cuda+fxt", when='@devel+starpu@svn-trunk+cuda+fxt')
    
    def cmake_setup(self):
        spec = self.spec
        stagedir=self.stage.path+'/qrm_gpu'
        with working_dir('spack-build',create=True):	
            
            ariths="d"
    
            if spec.satisfies('+s'):
                ariths += ",s"
            if spec.satisfies('+c'):
                ariths += ",c"
            if spec.satisfies('+z'):
                ariths += ",z"
   
            args = [".."]
            args = args + []
            args.extend([
                    "-Wno-dev",
                    "-DARITH=%s" % ariths,
                    "-DCMAKE_BUILD_TYPE=%s"    % ('Debug' if '+debug'  in spec else 'Release'),
                    "-DQRM_WITH_STARPU=%s"     % ('OFF'   if '-starpu' in spec else 'ON'     ),
                    "-DQRM_ORDERING_SCOTCH=%s" % ('ON'    if '+scotch' in spec else 'OFF'    ),
                    "-DQRM_ORDERING_METIS=%s"  % ('OFF'   if '-metis'  in spec else 'ON'     ),
                    "-DQRM_WITH_CUDA=%s"       % ('ON'    if '+cuda'   in spec else 'OFF'    )
                    ])
   
            # StarPU
            if spec.satisfies('+starpu'):
                args.extend(["-DSTARPU_DIR=%s" % spec['starpu'].prefix])  
            
            # Blas 
            blas_libs = spec['blas'].libs.ld_flags
            args.extend(["-DBLAS_LIBRARIES=%s" % blas_libs])
    
            # Lapack
            lapack_libs = spec['lapack'].libs.ld_flags
            args.extend(["-DLAPACK_LIBRARIES=%s" % lapack_libs])
                                       
            cmake(*args)
            make("install", parallel=False)


    def make_setup(self):
        spec = self.spec

        if spec.satisfies('@2.0'):
                stagedir=self.stage.path+'/qr_mumps-2.0'
        if spec.satisfies("%xl"):
                copyfile('makeincs/Make.inc.ibm', 'makeincs/Make.inc.spack')
        else:
                copyfile('makeincs/Make.inc.gnu', 'makeincs/Make.inc.spack')
        mf = FileFilter('makeincs/Make.inc.spack')

        mf.filter('topdir=\$\(HOME\)/path/to/here', 'topdir=%s/' % stagedir)

        mf.filter('^# CC      =.*', 'CC      = cc')
        mf.filter('^# FC      =.*', 'FC      = f90')

        includelist = ''
        definitions = ''
        hwloc = spec['hwloc'].prefix
        includelist += ' `pkg-config --cflags hwloc`'
        if spec.satisfies('+colamd'):
            colamd = spec['colamd'].prefix
            includelist += ' -I%s' %  colamd.include
            definitions += ' -Dhave_colamd'
        if spec.satisfies('+metis'):
            metis = spec['metis'].prefix
            includelist += ' -I%s' %  metis.include
            definitions += ' -Dhave_metis'
        if spec.satisfies('+scotch'):
            scotch = spec['scotch'].prefix
            mf.filter('FINCLUDES=.*', 'FINCLUDES= -I%s' % scotch.include)
            definitions += ' -Dhave_scotch'
        if spec.satisfies('+starpu'):
            starpu = spec['starpu'].prefix
            includelist += ' `pkg-config --cflags libstarpu`'
            definitions += ' -Dhave_starpu'
        if spec.satisfies('+fxt'):
            definitions += ' -Dhave_fxt'
        mf.filter('CINCLUDES=.*', 'CINCLUDES= %s' % includelist)
        mf.filter('CDEFS =.*', 'CDEFS = %s' % definitions)
        mf.filter('FDEFS =.*', 'FDEFS = %s' % definitions)

        optf = 'FCFLAGS  = -O3 -fPIC'
        optc = 'CFLAGS   = -O3 -fPIC'
        if spec.satisfies("%xl"):
            optf = 'FCFLAGS  = -O3 -qpic -qhot -qtune=auto -qarch=auto'
            optc = 'CFLAGS   = -O3 -qpic -qhot -qtune=auto -qarch=auto'
            mf.filter('^DEFINE_PREPEND =.*', 'DEFINE_PREPEND = -WF,')
            definitionsWF = definitions.replace('-D', '-WF,-D')
            mf.filter('FDEFS =.*', 'FDEFS = %s' % definitionsWF)
        if spec.satisfies('+debug'):
            optf += ' -g'
            optc += ' -g'
        if '^mkl' in spec:
            optf+= ' -m64 -I${MKLROOT}/include'
            optc+= ' -m64 -I${MKLROOT}/include'
        if spec.satisfies("%intel"):
            # The ifort default runtime is not thread safe. Adding the
            # -qopenmp flag ensures that thread-safe code is generated
            # and a thread-safe runtime is linked
            mf.filter('^LDFLAGS.*', 'LDFLAGS = $(FCFLAGS) -nofor_main -openmp')
            optf = 'FCFLAGS  = -O3 -fPIC -openmp'
            optc = 'CFLAGS   = -O3 -fPIC'
        mf.filter('^FCFLAGS =.*', '%s' % optf)
        mf.filter('^CFLAGS  =.*', '%s' % optc)

        # Blas 
        blas_libs = spec['blas'].libs.ld_flags
    
        # Lapack
        lapack_libs = spec['lapack'].libs.ld_flags

        mf.filter('^LBLAS    =.*', 'LBLAS    = %s' % blas_libs)
        mf.filter('^LLAPACK  =.*', 'LLAPACK  = %s' % lapack_libs)

        if spec.satisfies('+colamd'):
            colamd = spec['colamd']
            mf.filter('^# LCOLAMD  =.*', 'LCOLAMD  = %s' % colamd.libs.ld_flags)
            mf.filter('^# ICOLAMD  =.*', 'ICOLAMD  = -I%s' % colamd.prefix.include)
        if spec.satisfies('+metis'):
            metis = spec['metis']
            mf.filter('^# LMETIS   =.*', 'LMETIS   = %s' % metis.libs.ld_flags)
            mf.filter('^# IMETIS   =.*', 'IMETIS   = -I%s' % metis.prefix.include)
        if spec.satisfies('+scotch'):
            scotch = spec['scotch']
            mf.filter('^# LSCOTCH  =.*', 'LSCOTCH  = %s' % scotch.libs.ld_flags)
            mf.filter('^# ISCOTCH  =.*', 'ISCOTCH  = -I%s' % scotch.prefix.include)
        if spec.satisfies('+starpu'):
            mf.filter('^# LSTARPU =.*', 'LSTARPU = `pkg-config --libs libstarpu`')
            mf.filter('^# ISTARPU =.*', 'ISTARPU = `pkg-config --cflags libstarpu`')

 

    def install(self, spec, prefix):     
        if spec.satisfies('@devel'):
            args = self.cmake_setup()
        else:
            self.make_setup()
            if spec.satisfies('@2.0'):
                stagedir=self.stage.path+'/qr_mumps-2.0'
            elif spec.satisfies('@src'):
                stagedir=self.stage.path

            ariths="d"
    
            if spec.satisfies('+s'):
                ariths += ",s"
            if spec.satisfies('+c'):
                ariths += ",c"
            if spec.satisfies('+z'):
                ariths += ",z"
   
            make('setup', 'BUILD=build', 'PLAT=spack')
            with working_dir('build'):
                for a in ariths:
                    make('lib', 'BUILD=build', 'PLAT=spack', 'ARITH='+a,parallel=False)
                    make('examples', 'BUILD=build', 'PLAT=spack', 'ARITH='+a,parallel=False)
                    make('testing', 'BUILD=build', 'PLAT=spack', 'ARITH='+a,parallel=False)
    
                ## No install provided
                # install lib
                install_tree('lib', prefix.lib)
                # install headers
                mkdirp(prefix.include)
                for file in os.listdir("%s/build/include" % stagedir):
                        install("include/"+file, prefix.include)
                # install examples
                mkdirp('%s/examples' % prefix)
                for executable in ["qrm_front", "qrm_test"]:
                        for a in ariths:
                                install('examples/'+a+executable, '%s/examples/' % prefix)
                # install testing
                mkdirp('%s/testing' % prefix)
                for executable in ["qrm_testing", "qrm_testing"]:
                        for a in ariths:
                                install('testing/'+a+executable, '%s/testing/' % prefix)