##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *

class Quark(MakefilePackage):
    """Enables the dynamic execution of tasks with data dependencies in a multi-core, multi-socket, shared-memory environment."""
    homepage = "http://icl.cs.utk.edu/quark/index.html"
    url      = "http://icl.cs.utk.edu/projectsfiles/quark/pubs/quark-0.9.0.tgz"

    version('0.9.0', '52066a24b21c390d2f4fb3b57e976d08',
            url="http://icl.cs.utk.edu/projectsfiles/quark/pubs/quark-0.9.0.tgz")
    version('master', git='https://github.com/ecrc/quark.git', preferred=True)


    depends_on("hwloc@:1.999")

    def edit(self, spec, prefix):
        make_inc = FileFilter("make.inc")
        make_inc.filter('prefix=./install', 'prefix=%s' % prefix)
        make_inc.filter('^CFLAGS=.*', 'CFLAGS=-fPIC')
