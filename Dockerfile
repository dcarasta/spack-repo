FROM hpclib/gitlab-ubuntu
MAINTAINER Florent Pruvost florent.pruvost@inria.fr

USER root

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1 \
    SPACK_ROOT=/usr/local
ENV DEBIAN_FRONTEND noninteractive

# Install other common packages
RUN apt-get install -y gnupg2

# Install minimal spack dependencies
RUN apt-get install -y --no-install-recommends \
        autoconf \
        build-essential \
        ca-certificates \
        coreutils \
        curl \
        environment-modules \
        git \
        python \
        unzip \
        vim \
     && rm -rf /var/lib/apt/lists/*

# load spack environment on login
RUN        echo "source $SPACK_ROOT/share/spack/setup-env.sh" \
           > /etc/profile.d/spack.sh

# spack settings
# note: if you wish to change default settings, add files alongside
#       the Dockerfile with your desired settings. Then uncomment this line
#COPY       packages.yaml modules.yaml $SPACK_ROOT/etc/spack/

# install spack
RUN        curl -s -L https://api.github.com/repos/spack/spack/tarball \
           | tar xzC $SPACK_ROOT --strip 1
# note: at this point one could also run ``spack bootstrap`` to avoid
#       parts of the long apt-get install list above

# Install dependencies of the solverstack
RUN spack install boost
RUN spack install cmake
RUN spack install fftw
RUN spack install metis
RUN spack install mumps
RUN spack install openblas
RUN spack install openmpi
RUN spack install scotch~metis
RUN spack clean -a

#RUN chown -R gitlab:gitlab /home/gitlab/

#USER gitlab
# change the default shell to be bash
#SHELL ["/bin/bash", "-c"]

# image run hook: the -l will make sure /etc/profile environments are loaded
CMD        /bin/bash -l
