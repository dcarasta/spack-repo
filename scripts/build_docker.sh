#!/bin/sh

# This script builds a docker image based on the Dockerfile located in
# the root of https://gitlab.inria.fr/solverstack/spack-repo. It will
# install spack packages in an ubuntu rolling release. The spack
# packages installation will depend on the underlying architecture
# (broadwell, haswell, skylake, ...). Because the ci tests are
# performed on virtual machine of ci.inria.fr the targeted
# architecture is broadwell so that it is convenient to build this
# image directly on the fine architecture, thus to execute this script
# on one of "morse-ubuntu16-maphys morse-ubuntu16-scalfmm
# morse-ubuntu16-pastix morse-ubuntu16-chameleon".

docker build -t hpclib/spack .
